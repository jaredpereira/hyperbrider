var HDWalletProvider = require('truffle-hdwallet-provider')
let mnemonic = require('./secrets.json')

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*', // Match any network id
      rpc: {
        host: 'localhost',
        port: 8545
      }
    },
    rinkeby: {
      provider: new HDWalletProvider(mnemonic, 'https://rinkeby.infura.io/2FBsjXKlWVXGLhKn7PF7'),
      network_id: 4
    },
    kovan: {
      provider: new HDWalletProvider(mnemonic, 'https://kovan.infura.io/2FBsjXKlWVXGLhKn7PF7'),
      network_id: 42
    }
  }
}
