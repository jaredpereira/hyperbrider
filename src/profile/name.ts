import {View} from './index'
export const name:View<string> = {
  view: (archive):Promise<string | null> => {
    return new Promise((resolve, reject) => {
      return archive.get('/name', (err:Error, nodes:any) => {
        if(err) reject(err)
        if(!nodes[0]) return resolve(null)
        resolve(nodes[0].value.toString('utf8'))
      })
    })
  },

  write: (name: string) => {
    return [
      {
        type: 'put',
        key: 'name',
        value: name
      }
    ]
  }
}
